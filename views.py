"""
Routes and views for the flask application.
"""
import os
from datetime import datetime
from flask import render_template,Flask,flash,request,redirect,url_for,session
from sample import app
import MySQLdb
from flask_mysqldb import MySQL

from sample.models.TeacherDAO import TeacherDAO
from sample.models.Subject import Subject
from sample.models.TestDAO import TestDAO
from sample.models.StudentDAO import StudentDAO
from sample.models.QuestionAnswer import QuestionAnswer
from sample.models.StudentAnswer import StudentAnswer
#app=Flask(__name__)
qNo = 0
qlst = []
n = 0
app.secret_key = os.urandom(24)
@app.route('/')
@app.route('/home')
def home():
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )



@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )


@app.route('/about')
def about():
    """Renders the about page."""
 
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )


@app.route('/saveTeacher',methods=["GET","POST"])
def saveTeacher():
     if request.method == 'POST':
         name  = request.form['name']
         school = request.form['school']
         emailId = request.form['emailId']
         password = request.form['password']
         password1 = request.form['password1']

         print(name)
         print(password)
         print(password1)

         if(password == password1):
             tdao = TeacherDAO();
             tdao.saveTeacher(name,school,emailId,password)
             return  render_template('SelectSubject.html')

         else:
             msg = "Check your password"
             return render_template('teacherSignup.html',msg=msg)

     return render_template('teacherSignup.html')



@app.route('/teacherLogin',methods=["GET","POST"])
def teacherLogin():
    if request.method == 'POST':
         print("i am the teacher login function")
         emailId  = request.form['emailId']
         password = request.form['password']

         print(emailId)

         tdao = TeacherDAO();
         teacher = tdao.getTeacher(emailId,password)

         print(teacher)

         if(teacher):
             teacher_id = teacher[0][0]
             session['teacher_id'] = teacher_id
             print("I AM AFTER SESSION", teacher_id)

             return redirect(url_for('layout'))
         else:
              return render_template('teacherLogin.html',msg="invalid Credentials")
            # return render_template('layout.html')
    return render_template('teacherLogin.html')

@app.route('/addSubject',methods=["GET","POST"])
def addSubject():
    print("addSubject function")
    teacher_id = 0

    if request.method == 'POST':
        subName  = request.form['subject']
        if 'teacher_id' in session:
            teacher_id = session.get('teacher_id')
            teacher_id = session['teacher_id']
            print(teacher_id)
            tdao = TeacherDAO();
            sdao = Subject();
            subId = subName[0:3] + str(teacher_id)
            r = sdao.getSubName(subId)
            print(r)
            if(r != []):
                return render_template('SelectSubject.html',msg="Yo have already registered for this subject.")
            else:
                tdao.addSubject1(subId, subName, teacher_id, 0)
                print("i am r")
                print("testing")
                return redirect(url_for('allSubjects'))
                #return render_template("index.html")
        else:
            return "teacherID session issue"

@app.route('/allSubjects',methods=["GET","POST"])
def allSubjects():
    if 'teacher_id' in session:
            teacher_id = session.get('teacher_id')
            teacher_id = session['teacher_id']
            print(teacher_id)
            sdao = Subject();
            lst = sdao.getAllSubjects(teacher_id)
            print("here it is in allSubjects",lst)
            return render_template("AllSubjects.html",len1 = len(lst),lst=lst)

@app.route('/printAllSubjects',methods=["GET","POST"])
def printAllSubjects(lst):
    return render_template("AllSubjects.html",lst=lst)


@app.route('/testDetails',methods=["GET","POST"])
def testDetails():
    testNo = 0
    if request.method == 'POST':
         examTitle  = request.form['examTitle']
         subj  = request.form['subject']
         conductedOn  = request.form['conductedOn']
         startTime  = request.form['startTime']
         endTime  = request.form['endTime']
         noq  = request.form['noq']
         marks  = request.form['marks']

         if 'teacher_id' in session:
             teacher_id = session.get('teacher_id')
             teacher_id = session['teacher_id']
             print(teacher_id)

             sdao = Subject();
             lst = sdao.getSubId(subj,teacher_id)
             subId = lst[0][0]
             print(subId)

             s1dao = Subject();
             num = sdao.updateTestNo(subId)
             print(num)

             testNo = str(subId) + str(num)
             session['testNo'] = testNo

             tsdao = TestDAO();
             tsdao.saveTestDetails(testNo,examTitle,subj,conductedOn,startTime,endTime,noq,marks,teacher_id,subId)

         return  redirect(url_for('qNa'))
    return render_template("TestDetails.html")


@app.route('/qNa',methods=["GET","POST"])
def qNa():
    return render_template("questionAnswer.html")


@app.route('/saveQNA',methods=["GET","POST"])
def saveQNA():
    global qNo
    if request.form['text'] == 'next':
        question  = request.form['question']
        answer  = request.form['answer']
        marks = request.form['marks']
        if 'testNo' in session:
            testNo = session.get('testNo')
            testNo = session['testNo']
            print(testNo)

        qNo = qNo + 1
        qadao = QuestionAnswer()
        print("this is step1")
        qadao.saveQuestionAnswer(testNo,qNo,question,answer,marks)

        return  redirect(url_for('qNa'))
     
    elif request.form['text'] == 'finish':
        question  = request.form['question']
        answer  = request.form['answer']
        marks = request.form['marks']
        if 'testNo' in session:
            testNo = session.get('testNo')
            testNo = session['testNo']
            print(testNo)
        qNo = qNo + 1

        qadao = QuestionAnswer()
        print("this is step1")
        qadao.saveQuestionAnswer(testNo,qNo,question,answer,marks)

        return  redirect(url_for('viewTest'))

         


@app.route('/viewTest',methods=["GET","POST"])
def viewTest():
    if 'testNo' in session:
             testNo = session.get('testNo')
             testNo = session['testNo']
             print(testNo)
    qadao = QuestionAnswer()
    print("samp1")
    lst = qadao.getAllQuestions(testNo)
    print(lst)
    print("samp2")
    len1 = len(lst)
    print("samp3")
    return  render_template("viewTest.html",lst=lst,len1=len1,testNo=testNo)

@app.route('/edit',methods=["GET","POST"])
def edit():
    if request.method == 'POST':
        testNo = request.form['testNo']
        qNo = request.form['qNo']
        qdao = QuestionAnswer()
        qAlst = qdao.getQuestionAnswer(testNo,qNo)
        print(qAlst)
        question = qAlst[0][0]
        print(question)
        answer = qAlst[0][1]
        marks = qAlst[0][2]
        return render_template("editQA.html",question= question, answer = answer, marks = marks,testNo = testNo,qNo=qNo)

@app.route('/updateQA',methods=["GET","POST"])
def updateQA():
     if request.method == 'POST':
        question  = request.form['question']
        answer  = request.form['answer']
        marks = request.form['marks']
        testNo = request.form['testNo']
        qNo = request.form['qNo']

        qadao = QuestionAnswer()
        qadao.updateQuestionAnswer(testNo,qNo,question,answer,marks)
        return  redirect(url_for('viewTest'))

@app.route('/getTeacher',methods=["GET","POST"])
def getTeacher():
    tdao = TeacherDAO()
    if 'teacher_id' in session:
        teacher_id = session.get('teacher_id')
        teacher_id = session['teacher_id']
        print(teacher_id)
        res = tdao.getTeacherDetails(teacher_id)
        name = res[0][1]
        school = res[0][2]
        emailId = res[0][3]
        password = res[0][4]
        return render_template("UpdateTeacher.html",name=name,school=school,emailId=emailId,password=password,password1=password)

@app.route('/updateTeacher',methods=["GET","POST"])
def updateTeacher():
    if request.method == 'POST':
        name  = request.form['name']
        school = request.form['school']
        emailId = request.form['emailId']
        password = request.form['password']
        password1 = request.form['password1']

        print(name)
        print(password)
        print(password1)

        if(password == password1):
            tdao = TeacherDAO();
            if 'teacher_id' in session:
                teacher_id = session.get('teacher_id')
                teacher_id = session['teacher_id']
                print(teacher_id)
            tdao.updateTeacher(teacher_id,name,school,emailId,password)
            return  redirect(url_for('layout'))

@app.route('/saveStudent',methods=["GET","POST"])
def saveStudent():
     if request.method == 'POST':
         sname  = request.form['sname']
         rollNo  = request.form['rollNo']
         Class  = request.form['class']
         section = request.form['section']
         school = request.form['school']
         emailId = request.form['emailId']
         spassword = request.form['spassword']
         password1 = request.form['password1']

         print(sname)
         print(spassword)
         print(password1)

         if(spassword == password1):
             studao = StudentDAO();
             studao.saveStudent(sname,rollNo,Class,section,school,emailId,spassword)
             return  render_template('RegisterSubject.html')

         else:
             msg = "Check your password"
             return render_template('StudentSignUp.html',msg=msg)

     return render_template('StudentSignUp.html')

@app.route('/studentLogin',methods=["GET","POST"])
def studentLogin():
     if request.method == 'POST':
         print("i am the student login function")
         emailId  = request.form['emailId']
         spassword = request.form['spassword']

         print(emailId)

         studao = StudentDAO();
         student = studao.getStudent(emailId,spassword)

         print(student)

         if(student):
             student_id = student[0][0]
             session['student_id'] = student_id
             print("I AM AFTER SESSION", student_id)
             return redirect(url_for('studentLayout'))
             
     return render_template('studentLogin.html')

@app.route('/redirectSubject',methods=["GET","POST"])
def redirectSubject():
    return render_template("SelectSubject.html")

@app.route('/studentLayout',methods=["GET","POST"])
def studentLayout():
    return render_template('studentLayout.html')


@app.route('/registerSubject',methods=["GET","POST"])
def registerSubject():
    return render_template("RegisterSubject.html")

@app.route('/subjectRegister',methods=["GET","POST"])
def subjectRegister():
     student_id = 0
     if request.method == 'POST':
         subName = ""
         subId  = request.form['subId']
         if 'student_id' in session:
            student_id = session.get('student_id')
            student_id = session['student_id']
            print(student_id)
            sdao = Subject();
            studao = StudentDAO();
            r = sdao.getSubName(subId)
          
            if(r != []):
                r = r[0][0]
                subName = r
                studao.subjectRegister(student_id,subId,subName)
                print("i am r")
                print("testing")
                return redirect(url_for('allStudentSubjects'))
            else:
                msg = "Enter the correct subject code"
                return render_template("RegisterSubject.html",msg=msg)
            #return render_template("index.html")
         else:
             return "studentID session issue"
     return render_template("RegisterSubject.html")


@app.route('/allStudentSubjects',methods=["GET","POST"])
def allStudentSubjects():
    if 'student_id' in session:
            student_id = session.get('student_id')
            student_id = session['student_id']
            print(student_id)
            sdao = StudentDAO();
            lst = sdao.getAllSubjects(student_id)
            print("here it is in allSubjects",lst)
            return render_template("AllSubjects.html",len1 = len(lst),lst=lst)
    

@app.route('/getStudent',methods=["GET","POST"])
def getStudent():
    studao = StudentDAO()
    if 'student_id' in session:
        student_id = session.get('student_id')
        student_id = session['student_id']
        print(student_id)
        res = studao.getStudentDetails(student_id)
        sname = res[0][1]
        rollNo = res[0][2]
        Class = res[0][3]
        section = res[0][4]
        school = res[0][5]
        emailId = res[0][6]
        spassword = res[0][7]
        return render_template("UpdateStudent.html",sname=sname,rollNo=rollNo,Class= Class,section = section, school=school,emailId=emailId,spassword=spassword,password1=spassword)



@app.route('/updateStudent',methods=["GET","POST"])
def updateStudent():
    print("entered the updateStudent")
    if request.method == 'POST':
        sname  = request.form['sname']
        rollNo =  request.form['rollNo']
        Class =  request.form['Class']
        section =  request.form['section']
        school = request.form['school']
        emailId = request.form['emailId']
        spassword = request.form['spassword']
        password1 = request.form['password1']

        print(sname)
        print(spassword)
        print(password1)

        if(spassword == password1):
            studao = StudentDAO();
            if 'student_id' in session:
                student_id = session.get('student_id')
                student_id = session['student_id']
                print(student_id)
                studao.updateStudent(student_id,sname,rollNo,Class,section,school,emailId,spassword)
            return  redirect(url_for('studentLayout'))


@app.route('/test',methods=["GET","POST"])
def test():
    if 'student_id' in session:
        student_id = session.get('student_id')
        student_id = session['student_id']
        print(student_id)
    tdao = TestDAO()
    print("this is the test function")
    lst = tdao.getTestNo(student_id)
    print(lst)
    return render_template("Tests.html",len1 = len(lst),lst=lst)

@app.route('/openTest',methods=["GET","POST"])
def openTest():
    global qlst
    print("entered the openTest")
    if request.method == 'POST':
        testNo  = request.form['testNo']
        print(testNo)
        qdao = QuestionAnswer()

        qlst = qdao.getQuestion(testNo)
        
        return render_template("TestPage.html",q = qlst[0][0],ques=qlst[0][1],marks=qlst[0][2],testNo = testNo)

@app.route('/submitAnswers',methods=["GET","POST"])
def submitAnswers():
    global n
    global qlst
    print("here submit d answers")

    if 'student_id' in session:
        student_id = session.get('student_id')
        student_id = session['student_id']
        print(student_id)

    if request.method == 'POST':
        ans = request.form['ans']
        qNo = request.form['quesNo']
        testNo = request.form['testNo']
        #i have hardcoded this scoresvalue. must call the model here.. get the marks and then add it to the table.
        scores = 2
        sadao = StudentAnswer()
        print(ans)
        res = sadao.getAnswer(student_id,testNo,qNo)
        if(res == []):
            sadao.saveAnswer(student_id,testNo,qNo,ans,scores)
        else:
            sadao.updateAnswer(student_id,testNo,qNo,ans,scores)
        
        n = n + 1
        print(n)

        if(n < len(qlst)):
            return render_template("TestPage.html",q = qlst[n][0],ques=qlst[n][1],marks=qlst[n][2],testNo=testNo)
        else:
            return  redirect(url_for('studentLayout'))


   



    

           

         


    








       

@app.route('/viewAllTest',methods=["GET","POST"])
def viewAllTest():
    if 'teacher_id' in session:
        teacher_id = session.get('teacher_id')
        teacher_id = session['teacher_id']
        print(teacher_id)
        tdao = TestDAO()
        lst = tdao.getTeacherTest(teacher_id)
        testNo = lst[0][0]
        return render_template("TeacherTests.html",len1 = len(lst),lst=lst,testNo=testNo)


@app.route('/questionAnswers/<testNo>',methods=["GET","POST"])
def questionAnswers(testNo):
    qdao = QuestionAnswer();
    res = qdao.finalQuestionAnswers(testNo)
    return render_template("testQuestionAnswer.html",len1= len(res),res= res)
 

@app.route('/helpTeacher',methods=["GET","POST"])
def helpTeacher():
      return render_template("contact.html")

@app.route('/logout',methods=["GET","POST"])
def logout():
      return render_template("index.html")




@app.route('/classScores',methods=["GET","POST"])
def classScores():
    return render_template('studentLayout.html')   

@app.route('/viewAllMyTest',methods=["GET","POST"])
def viewAllMyTest():
    if 'student_id' in session:
        student_id = session.get('student_id')
        student_id = session['student_id']
        print(student_id)
    tdao = TestDAO()
    print("this is the test function")
    lst = tdao.getTests(student_id)
    print(lst)
    testNo = lst[0][0]
    return render_template("TeacherTests.html",len1 = len(lst),lst=lst,testNo=testNo)




@app.route('/myScores',methods=["GET","POST"])
def myScores():
    return render_template('studentLayout.html')  

@app.route('/helpStudent',methods=["GET","POST"])
def helpStudent():
      return render_template("contact.html")



@app.route('/layout',methods=["GET","POST"])
def layout():
    return render_template('layout.html')







if __name__ == "main":
    x = 0
    app.run()
    