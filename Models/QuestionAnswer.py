import pypyodbc
class QuestionAnswer(object):
    """description of class"""
    def __init__(self):
         connectionString = 'Driver={ODBC Driver 13 for SQL Server};Server=tcp:easecheck.database.windows.net,1433;Database=trial;Uid=sonu@easecheck;Pwd=p@ssw0rd;Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;'
         self.connection = pypyodbc.connect(connectionString);
         self.cursor = self.connection.cursor();
         print("Connection Established")
         return;
    def saveQuestionAnswer(self,testNo,qNo,question,answer,marks):
         print("step2")
         sql = "INSERT INTO questionAnswer(testNo,qNo,question,answer,marks) VALUES(?,?,?,?,?)";
         values = [testNo,qNo,question,answer,marks];
         self.cursor.execute(sql,values);
         self.connection.commit();
         self.connection.close();
         print("this is inserted")
         return;
    def getAllQuestions(self,testNo):
         sql = "SELECT qNo,question,answer,marks FROM questionAnswer WHERE testNo = ?";
         values=[testNo]
         self.cursor.execute(sql, values);
         results = self.cursor.fetchall();
         print("here its in DAO",results)
         return results

    def getQuestionAnswer(self,testNo,qNo):
         sql = "SELECT question,answer,marks FROM questionAnswer WHERE testNo = ? and qNo = ?";
         values=[testNo,qNo]
         self.cursor.execute(sql, values);
         results = self.cursor.fetchall();
         print("here its in DAO",results)
         return results

    def updateQuestionAnswer(self,testNo,qNo,question,answer,marks):
        sql = "UPDATE questionAnswer SET question = ? ,answer = ?, marks = ? WHERE testNo = ? and qNo = ?";
        values=[question,answer,marks,testNo,qNo]
        self.cursor.execute(sql, values)
        
        self.connection.commit()
        self.connection.close()
        return

    def getQuestion(self,testNo):
        sql = "SELECT qNo,question,marks FROM questionAnswer WHERE testNo = ?";
        values=[testNo]
        self.cursor.execute(sql, values);
        results = self.cursor.fetchall();
        print("here its in DAO",results)
        return results

    def finalQuestionAnswers(self,testNo):
        sql = "SELECT qNo,question,answer,marks FROM questionAnswer WHERE testNo = ?";
        values=[testNo]
        self.cursor.execute(sql, values);
        results = self.cursor.fetchall();
        print("here its in DAO",results)
        return results






