# EaseCheck

Automatic Question Evaluation using Python ML

Install below pacakges when you run the project and then have a virtual environment.
Click(7.0)
Flask(0.12.4)
Flask-MySQL(1.4.0)
Flask-MySQLdb(1.2.0)
itsdangerous(1.1.0)
Jinja2(2.10)
MarkupSafe(1.1.0)
mysqlclient(1.4.1)
pip(10.0.1)
PyMySQL(0.9.3)
pypyodbc(1.3.4)
setuptools(39.0.1)
Werkzeug(0.14.1)